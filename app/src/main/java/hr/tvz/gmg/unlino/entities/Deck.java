package hr.tvz.gmg.unlino.entities;

import android.content.Context;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Robin on 12-Apr-16.
 */
public class Deck {

    private int numberOfLocations;
    private int numberOfJobs;
    private Context mContext;

    private ArrayList<DeckJobs> locationDecks;

    public Deck(Context context) {
        this.numberOfLocations = 27;
        this.numberOfJobs = 7;
        this.mContext = context;
    }

    public void generateDeck(){
        //generate everyLocation
        locationDecks = new ArrayList<>();
        for (int i=0; i<numberOfLocations; i++){
            DeckJobs tmp = new DeckJobs(i, mContext);
            locationDecks.add(tmp);
        }
    }

    public DeckJobs getRandomLocation(){
        Random r = new Random();
        int rand = r.nextInt(27);
        return locationDecks.get(rand);
    }

    public ArrayList<DeckJobs> getLocationDecks() {
        return locationDecks;
    }
}
