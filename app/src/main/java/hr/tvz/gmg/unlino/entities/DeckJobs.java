package hr.tvz.gmg.unlino.entities;

import android.content.Context;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import hr.tvz.gmg.unlino.R;

/**
 * Created by Robin on 07-Jun-16.
 */
public class DeckJobs {
    private static Context mContext;

    //private String location;
    private int imageId;
    ArrayList<String> jobs;
    ArrayList<String> everyLocation; // this gives location due to a number, from string array
    String location; // unique location of this deck

    public DeckJobs(int loc, Context context) {
        this.mContext = context;
        String[] tmp1 = mContext.getResources().getStringArray(R.array.deck_locations);
        List<String> tmp2  = Arrays.asList(tmp1);
        this.everyLocation = new ArrayList<>(tmp2);

        switch (loc){
            case 0:
                generateJobs(R.array.theater_jobs, 0, R.drawable.card_theater);
                break;
            case 1:
                /*jobs = new ArrayList<>(Arrays.asList(mContext.getResources().getStringArray(R.array.restaurant_jobs)));
                setLocation(everyLocation.get(1));
                setImageId(R.drawable.card_restaurant);*/
                generateJobs(R.array.restaurant_jobs, 1, R.drawable.card_restaurant);
                break;
            case 2:
                generateJobs(R.array.beach_jobs, 2, R.drawable.card_beach);
                break;
            case 3:
                generateJobs(R.array.school_jobs, 3, R.drawable.card_school);
                break;
            case 4:
                generateJobs(R.array.supermarket_jobs, 4, R.drawable.card_supermarket);
                break;
            case 5:
                generateJobs(R.array.service_station_jobs, 5, R.drawable.card_service_station);
                break;
            case 6:
                generateJobs(R.array.circus_tent_jobs, 6, R.drawable.card_circus_tent);
                break;
            case 7:
                generateJobs(R.array.hospital_jobs, 7, R.drawable.card_hospital);
                break;
            case 8:
                generateJobs(R.array.crusader_army_jobs, 8, R.drawable.card_crusader_army);
                break;
            case 9:
                generateJobs(R.array.passenger_train_jobs, 9, R.drawable.card_passenger_train);
                break;
            case 10:
                generateJobs(R.array.bank_jobs, 10, R.drawable.card_bank);
                break;
            case 11:
                generateJobs(R.array.airplane_jobs, 11, R.drawable.card_airplane);
                break;
            case 12:
                generateJobs(R.array.police_station_jobs, 12, R.drawable.card_police_station);
                break;
            case 13:
                generateJobs(R.array.pirate_ship_jobs, 13, R.drawable.card_pirate_ship);
                break;
            case 14:
                generateJobs(R.array.movie_studio_jobs, 14, R.drawable.card_movie_studio);
                break;
            case 15:
                generateJobs(R.array.military_base_jobs, 15, R.drawable.card_military_base);
                break;
            case 16:
                generateJobs(R.array.submarine_jobs, 16, R.drawable.card_submarine);
                break;
            case 17:
                generateJobs(R.array.ocean_liner_jobs, 17, R.drawable.card_ocean_liner);
                break;
            case 18:
                generateJobs(R.array.polar_station_jobs, 18, R.drawable.card_polar_station);
                break;
            case 19:
                generateJobs(R.array.hotel_jobs, 19, R.drawable.card_hotel);
                break;
            case 20:
                generateJobs(R.array.corporate_party_jobs, 20, R.drawable.card_corporate_party);
                break;
            case 21:
                generateJobs(R.array.day_spa_jobs, 21, R.drawable.card_day_spa);
                break;
            case 22:
                generateJobs(R.array.university_jobs, 22, R.drawable.card_university);
                break;
            case 23:
                generateJobs(R.array.embassy_jobs, 23, R.drawable.card_embassy);
                break;
            case 24:
                generateJobs(R.array.space_station_jobs, 24, R.drawable.card_space_station);
                break;
            case 25:
                generateJobs(R.array.casino_jobs, 25, R.drawable.card_casino);
                break;
            case 26:
                //generateJobs(R.array.cathedral_jobs, 26, R.drawable.card_cathedral);
                break;
        }

    }

    private void generateJobs(int stringResource, int n, int imageId){
        setJobs(new ArrayList<>(Arrays.asList(mContext.getResources().getStringArray(stringResource))));
        setLocation(everyLocation.get(n));
        setImageId(imageId);
    }

    public ArrayList<Job> getNJobsAndASpy (int n){
        int deckSize = 7;
        Random r = new Random();
        ArrayList<Job> newJobs = new ArrayList<>();

        // testing for non-spy host
        /*newJobs.add(new Job(
                this.jobs.get(r.nextInt(deckSize)),
                getImageId(),
                false
        ));
        return newJobs;*/

        if (n == 1){
            newJobs.add(new Job(mContext.getResources().getString(R.string.deckSpy), R.drawable.card_spy, true));
            return newJobs;
        }

        for (int i=0; i<n-1; i++){
            int rand = r.nextInt(deckSize);
            newJobs.add(new Job(this.jobs.get(i), getImageId(), false));
            this.jobs.remove(i);
            deckSize--;
        }
        newJobs.add(new Job(mContext.getResources().getString(R.string.deckSpy), R.drawable.card_spy, true));
        Collections.shuffle(newJobs, new Random());
        return newJobs;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public int getImageId() {
        return imageId;
    }

    public void setJobs(ArrayList<String> jobs) {
        this.jobs = jobs;
    }
}
