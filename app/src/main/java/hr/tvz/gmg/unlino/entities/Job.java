package hr.tvz.gmg.unlino.entities;

/**
 * Created by Robin on 07-Jun-16.
 */
public class Job {
    private String profession;
    private boolean spy;
    private int imageId;

    public Job(String profession, int imageId, boolean spy) {
        this.profession = profession;
        this.imageId = imageId;
        this.spy = spy;
    }

    public String getProfession() {
        return profession;
    }

    public boolean isSpy() {
        return spy;
    }

    public int getImageId() {
        return imageId;
    }



}
