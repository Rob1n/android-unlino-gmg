package hr.tvz.gmg.unlino.activities;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import hr.tvz.gmg.unlino.R;
import hr.tvz.gmg.unlino.adapters.RecyclerViewHorizontalAdapter;
import hr.tvz.gmg.unlino.entities.Constants;
import hr.tvz.gmg.unlino.entities.DeckJobs;
import hr.tvz.gmg.unlino.entities.Deck;
import hr.tvz.gmg.unlino.entities.Job;
import hr.tvz.gmg.unlino.networking.ParcelableBluetoothDevices;
import hr.tvz.gmg.unlino.networking.ParcelableBluetoothSockets;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import android.app.AlertDialog;
import android.content.DialogInterface;

public class GameHostActivity extends AppCompatActivity {

    private TextView textViewJob;
    private TextView textViewTimer;
    private LinearLayout linearLayoutWithImage;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager recyclerViewLayoutManager;

    private ArrayList<BluetoothDevice> connectedDevices;
    private ArrayList<BluetoothSocket> connectedSockets;
    private ArrayList<ConnectedThread> connectionThreads;

    CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // custom font
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/PTSansNarrow.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_game);

        connectedDevices = ((ParcelableBluetoothDevices) getIntent().getExtras().getParcelable("hostConnectedDevices")).getDevices();
        connectedSockets = ((ParcelableBluetoothSockets) getIntent().getExtras().getParcelable("hostConnectedSockets")).getSockets();
        connectionThreads = new ArrayList<>();

        textViewJob = (TextView) findViewById(R.id.textViewGameJob);
        textViewTimer = (TextView) findViewById(R.id.textViewTimer);
        linearLayoutWithImage = (LinearLayout) findViewById(R.id.linearLayoutLocationImage);

        //connecting to devices
        for (BluetoothSocket socket : connectedSockets){
            ConnectedThread thread = new ConnectedThread(socket);
            connectionThreads.add(thread);
        }

        //add players to lobby and send a RecyclerViewHorizontalAdapter message
        /*for (ConnectedThread thread : connectionThreads){
            thread.write("bok".getBytes());
        }*/

        // organize the game
        int numberOfPlayers = connectedSockets.size() + 1;
        //TODO return value when finished testing
        if (numberOfPlayers < 0){
            Toast.makeText(this, "Nedovoljan broj igraca!", Toast.LENGTH_SHORT).show();
            finish();
        }

        // create decks
        Deck completeDeck = new Deck(this);
        completeDeck.generateDeck();
        DeckJobs singleLocation = completeDeck.getRandomLocation();
        ArrayList<Job> generatedJobs = singleLocation.getNJobsAndASpy(numberOfPlayers);

        // give players a job
        int pos = 0;
        for (ConnectedThread thread : connectionThreads){
            Job job = generatedJobs.get(pos);
            String jobString = "J"+job.getProfession();
            thread.write(jobString.getBytes());
            String imageString = "I"+job.getImageId();
            thread.write(imageString.getBytes());
            String spyString;
            if (job.isSpy()){
                spyString = "ST";
            } else {
                spyString = "SF";
            }
            thread.write(spyString.getBytes());
            pos++;
        }

        // give host a job
        Job hostJob = generatedJobs.get(pos);
        textViewJob.setText(hostJob.getProfession());
        linearLayoutWithImage.setBackgroundResource(hostJob.getImageId());

        //if spy color the job differently
        int styleId = R.style.jobTextOutlineRegular;
        if (hostJob.isSpy())
            styleId = R.style.jobTextOutlineSpy;

        if (Build.VERSION.SDK_INT < 23){
            //noinspection deprecation
            textViewJob.setTextAppearance(this, styleId);
        } else {
            textViewJob.setTextAppearance(styleId);
        }


        // start the timer
        SharedPreferences sharedPreferences = this.getSharedPreferences(Constants.SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE);
        // duration saved in minutes
        int savedDuration = sharedPreferences.getInt(Constants.spKeyDuration, Constants.defaultDuration);
        long timeRemaining = savedDuration*60*1000;
        countDownTimer = new CountDownTimer(timeRemaining, Constants.COUNT_DOWN_INTERVAL) {

            @Override
            public void onTick(long millisUntilFinished) {
                String tmp = String.format("%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))
                        );
                textViewTimer.setText(tmp);

                if (millisUntilFinished < (60*1000)){
                    textViewTimer.setTextColor(getResources().getColor(R.color.red));
                }
            }

            @Override
            public void onFinish() {
                Toast.makeText(GameHostActivity.this, "Time ran out!", Toast.LENGTH_LONG).show();
                //play alert sound
                try {
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                    r.play();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        countDownTimer.start();

        // create the horizontal recycler view with all locations
        recyclerView = (RecyclerView) findViewById(R.id.gameRecyclerView);
        recyclerView.setHasFixedSize(true);

        // layout manager
        recyclerViewLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(recyclerViewLayoutManager);

        // adapter
        RecyclerViewHorizontalAdapter recyclerViewAdapter;
        recyclerViewAdapter = new RecyclerViewHorizontalAdapter(completeDeck);
        recyclerView.setAdapter(recyclerViewAdapter);

    }

    private Handler mHandler = new Handler(){

        public void handleMessage(Message msg){
            switch (msg.what) {
                case Constants.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    //TODO after writing message do what?
                    //mConversationArrayAdapter.add("Me:  " + writeMessage);

                    break;
                case Constants.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    //TODO after reading message do what?
                    //mConversationArrayAdapter.add(mConnectedDeviceName + ":  " + readMessage);
                    break;
            }
        }
    };

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[1024];  // buffer store for the stream
            int bytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {
                    // Read from the InputStream
                    bytes = mmInStream.read(buffer);
                    // Send the obtained bytes to the UI activity
                    // TODO update info
                    mHandler.obtainMessage(Constants.MESSAGE_READ, bytes, -1, buffer)
                            .sendToTarget();



                } catch (IOException e) {
                    break;
                }
            }
        }

        /* Call this from the main activity to send data to the remote device */
        public void write(byte[] bytes) {
            try {
                mmOutStream.write(bytes);
            } catch (IOException e) { }
        }

        /* Call this from the main activity to shutdown the connection */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) { }
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to exit?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        countDownTimer.cancel();
                        GameHostActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .create().show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (countDownTimer != null)
            countDownTimer.cancel();
    }
}
