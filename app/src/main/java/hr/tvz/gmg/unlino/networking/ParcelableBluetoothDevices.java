package hr.tvz.gmg.unlino.networking;

import android.bluetooth.BluetoothDevice;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Robin on 06-Jun-16.
 */
public class ParcelableBluetoothDevices implements Parcelable {

    ArrayList<BluetoothDevice> devices;

    public ParcelableBluetoothDevices(ArrayList<BluetoothDevice> dev){
        this.devices = dev;
    }

    public ArrayList<BluetoothDevice> getDevices() {
        return devices;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.devices);
    }

    protected ParcelableBluetoothDevices(Parcel in) {
        this.devices = in.createTypedArrayList(BluetoothDevice.CREATOR);
    }

    public static final Parcelable.Creator<ParcelableBluetoothDevices> CREATOR = new Parcelable.Creator<ParcelableBluetoothDevices>() {
        @Override
        public ParcelableBluetoothDevices createFromParcel(Parcel source) {
            return new ParcelableBluetoothDevices(source);
        }

        @Override
        public ParcelableBluetoothDevices[] newArray(int size) {
            return new ParcelableBluetoothDevices[size];
        }
    };
}
