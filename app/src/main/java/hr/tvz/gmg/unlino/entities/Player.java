package hr.tvz.gmg.unlino.entities;

import java.util.ArrayList;

/**
 * Created by Robin on 12-Apr-16.
 */
public class Player {

    private String playerID;
    private String playerUsername;
    private String playerDeviceName;
    private boolean host;

    private ArrayList<Card> hand;
    private int score;


    public int calculateScore(){
        int score = 0;
        for (Card card : hand){
            score += card.getValue();
        }
        return score;
    }

    public int getHandSize(){
        return hand.size();
    }

}
