package hr.tvz.gmg.unlino.activities;

import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import hr.tvz.gmg.unlino.R;
import hr.tvz.gmg.unlino.entities.Constants;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class GameClientActivity extends AppCompatActivity {

    private TextView textViewJob;
    private LinearLayout linearLayoutWithImage;

    private BluetoothDevice mBluetoothDevice;
    private BluetoothSocket mBluetoothSocket;
    private ConnectedThread mConnectionThread;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // custom font
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/PTSansNarrow.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_game);

        textViewJob = (TextView) findViewById(R.id.textViewGameJob);
        linearLayoutWithImage = (LinearLayout) findViewById(R.id.linearLayoutLocationImage);


    }


    private Handler mHandler = new Handler(){

        public void handleMessage(Message msg){
            switch (msg.what) {
                case Constants.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    //TODO after writing message do what?
                    //mConversationArrayAdapter.add("Me:  " + writeMessage);

                    break;
                case Constants.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    //TODO after reading message do what?
                    //mConversationArrayAdapter.add(mConnectedDeviceName + ":  " + readMessage);
                    processReadInformation(readMessage);
                    break;
            }
        }
    };

    /**
     * method that sets all the necessary info that client needs to know about the game, after it's read from the bluetooth thread
     * @param text received data in string format
     */
    private void processReadInformation(String text){
        // need to parse received text
        if (text.substring(0,0).equals("J"))
        {
            // J is for job information (String profession)
            this.textViewJob.setText(text.substring(1, text.length()));
        }
        else if (text.substring(0,0).equals("I"))
        {
            // I is for image information (int imageId)
            String imageIdStr = text.substring(1, text.length());
            int imageId = Integer.parseInt(imageIdStr);
            linearLayoutWithImage.setBackgroundResource(imageId);
        }
        else if (text.substring(0,0).equals("S"))
        {
            // S is for spy information (boolean true if T, false if F)
            int styleId = R.style.jobTextOutlineRegular;
            if (text.substring(1,1).equals("T"))
                styleId = R.style.jobTextOutlineSpy;

            if (Build.VERSION.SDK_INT < 23){
                //noinspection deprecation
                textViewJob.setTextAppearance(this, styleId);
            } else {
                textViewJob.setTextAppearance(styleId);
            }
        }
        else
        {
            Log.e("Log-GameClientActivity", "client read invalid data: " + text);
        }
    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[1024];  // buffer store for the stream
            int bytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {
                    // Read from the InputStream
                    bytes = mmInStream.read(buffer);
                    // Send the obtained bytes to the UI activity
                    // TODO update info
                    mHandler.obtainMessage(Constants.MESSAGE_READ, bytes, -1, buffer)
                            .sendToTarget();


                } catch (IOException e) {
                    break;
                }
            }
        }

        /* Call this from the main activity to send data to the remote device */
        public void write(byte[] bytes) {
            try {
                mmOutStream.write(bytes);
            } catch (IOException e) { }
        }

        /* Call this from the main activity to shutdown the connection */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) { }
        }


    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to exit?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        GameClientActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .create().show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
