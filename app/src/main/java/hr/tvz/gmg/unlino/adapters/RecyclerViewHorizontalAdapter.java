package hr.tvz.gmg.unlino.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import hr.tvz.gmg.unlino.R;
import hr.tvz.gmg.unlino.entities.Deck;

/**
 * Created by Robin on 07-Jun-16.
 */
public class RecyclerViewHorizontalAdapter extends RecyclerView.Adapter<RecyclerViewHorizontalAdapter.MyViewHolder>{

    private Deck mDeckData;

    public RecyclerViewHorizontalAdapter(Deck mDeckData) {
        this.mDeckData = mDeckData;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView mImageView;

        public MyViewHolder(View view) {
            super(view);
            mImageView = (ImageView) view.findViewById(R.id.imageViewRecycleItemHorizontal);
        }
    }

    // Usually involves inflating a layout from XML and returning the holder
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        //inflate the custom layout
        View itemView = inflater.inflate(R.layout.item_recycle_view_horizontal, parent, false);

        // return a new holder instance
        MyViewHolder viewHolder = new MyViewHolder(itemView);
        return viewHolder;
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        //DeckJobs deckJobs = mDeckData.get(position);

        int imageId = mDeckData.getLocationDecks().get(position).getImageId();

        ImageView imageView = holder.mImageView;
        imageView.setImageResource(imageId);
    }

    @Override
    public int getItemCount() {
        return mDeckData.getLocationDecks().size();
    }
}
