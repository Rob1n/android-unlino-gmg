package hr.tvz.gmg.unlino.activities;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import hr.tvz.gmg.unlino.R;
import hr.tvz.gmg.unlino.adapters.PairedDevicesAdapter;
import hr.tvz.gmg.unlino.entities.Constants;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BluetoothJoinActivity extends AppCompatActivity {
    //@BindView(R.id.listViewJoinPairedDevices) ListView listViewPairedDevices;

    ListView listViewPairedDevices;
    Button btnRefreshDevices;

    private BluetoothAdapter mBluetoothAdapter;
    private BroadcastReceiver mReceiver;
    private BluetoothServerSocket mmServerSocket;

    private ArrayList<BluetoothDevice> foundDevices;
    private PairedDevicesAdapter pairedDevicesAdapter;
    private BluetoothDevice connectedDevice;
    private BluetoothSocket mBluetoothSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // custom font
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/PTSansNarrow.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_bluetooth_join);
        //ButterKnife.bind(this);

        listViewPairedDevices = (ListView) findViewById(R.id.listViewJoinPairedDevices);
        btnRefreshDevices = (Button) findViewById(R.id.btnJoinRefreshDevices);

        // initialize bluetooth
        this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null){
            // bluetooth not supported
            Toast.makeText(getApplicationContext(), "Bluetooth is not supported on this device", Toast.LENGTH_SHORT).show();
            finish();
        }

        // initializing listview of paired devices
        foundDevices = new ArrayList<>();
        pairedDevicesAdapter = new PairedDevicesAdapter(getApplicationContext(), foundDevices);
        listViewPairedDevices.setAdapter(pairedDevicesAdapter);

        // getting already paired devices
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0){
            for (BluetoothDevice device : pairedDevices){
                foundDevices.add(device);
            }
        }
        pairedDevicesAdapter.notifyDataSetChanged();

        // enable bluetooth and start discovery
        if (!mBluetoothAdapter.isEnabled()) {
            // enable bluetooth
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, Constants.REQUEST_ENABLE_BT);
        } else {
            discoverDevices();
        }


        btnRefreshDevices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
                foundDevices = new ArrayList<>();
                if (pairedDevices.size() > 0){
                    for (BluetoothDevice device : pairedDevices){
                        foundDevices.add(device);
                    }
                }
                pairedDevicesAdapter.notifyDataSetChanged();
            }
        });

        // clicking on a paired device starts a connection thread with it
        listViewPairedDevices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // connect to selected paired device
                BluetoothDevice selectedDevice = foundDevices.get(position);
                if (selectedDevice != null){
                    ConnectThread connectThread = new ConnectThread(selectedDevice);
                    connectThread.start();

                    // prepare the game screen
                    Intent i = new Intent(getApplicationContext(), GameClientActivity.class);
                    startActivity(i);
                }

            }
        });



    }

    private void manageConnectedSocket(BluetoothSocket socket){
        this.connectedDevice = socket.getRemoteDevice();
        this.mBluetoothSocket = socket;
        Toast.makeText(getApplicationContext(), "Connected to device: "+connectedDevice.getName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_ENABLE_BT){
            if (resultCode == RESULT_OK){
                //bluetooth successfully enabled
                discoverDevices();
            }
        }
    }

    private void discoverDevices (){

        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                // When discovery finds a device
                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    // Get the BluetoothDevice object from the Intent
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    // Add the name and address to an array adapter to show in a ListView
                    //foundDevices.add(new ParcelableBluetoothDevice(device));
                    //foundDevices.add(device.getName() + "\n" + device.getAddress());

                    //refresh recycleView with new data
                    foundDevices.add(device);
                    pairedDevicesAdapter.notifyDataSetChanged();
                    //recyclerViewAdapter.updateList(foundDevices);
                }
            }
        };

        // Register the BroadcastReceiver
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, filter); // Don't forget to unregister during onDestroy

        mBluetoothAdapter.startDiscovery();

    }

    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device) {
            // Use a temporary object that is later assigned to mmSocket,
            // because mmSocket is final
            BluetoothSocket tmp = null;
            mmDevice = device;

            // Get a BluetoothSocket to connect with the given BluetoothDevice
            try {
                // MY_UUID is the app's UUID string, also used by the server code
                tmp = device.createRfcommSocketToServiceRecord(Constants.MY_UUID);
            } catch (IOException e) { }
            mmSocket = tmp;
        }

        public void run() {
            // Cancel discovery because it will slow down the connection
            mBluetoothAdapter.cancelDiscovery();

            try {
                // Connect the device through the socket. This will block
                // until it succeeds or throws an exception
                mmSocket.connect();
            } catch (IOException connectException) {
                // Unable to connect; close the socket and get out
                try {
                    mmSocket.close();
                } catch (IOException closeException) { }
                return;
            }

            // Do work to manage the connection (in a separate thread)
            manageConnectedSocket(mmSocket);
        }

        // Will cancel an in-progress connection, and close the socket
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) { }
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
        finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
