package hr.tvz.gmg.unlino.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.Image;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import hr.tvz.gmg.unlino.R;
import hr.tvz.gmg.unlino.entities.Constants;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    private Button mainMenuBluetoothHost;
    private Button mainMenuBluetoothJoin;
    //private ImageButton mainMenuRules;
    //private ImageButton mainMenuSettings;
    private TextView tv;
    private TextView tvUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // custom font
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/PTSansNarrow.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextAppearance(this, R.style.titleText);
        setSupportActionBar(toolbar);

        this.mainMenuBluetoothHost = (Button) findViewById(R.id.mainMenuBluetoothHost);
        this.mainMenuBluetoothJoin = (Button) findViewById(R.id.mainMenuBluetoothJoin);
        //this.mainMenuRules = (ImageButton) findViewById(R.id.imgBtnHelp);
        //this.mainMenuSettings = (ImageButton) findViewById(R.id.imgBtnSettings);
        this.tvUsername = (TextView) findViewById(R.id.textViewMainUsername);

        //username
        SharedPreferences sharedPreferences = this.getSharedPreferences(Constants.SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE);
        String username = sharedPreferences.getString(Constants.spKeyUsername, Constants.defaultUsername);
        tvUsername.setText(this.getResources().getString(R.string.greeting)+" "+username);

        mainMenuBluetoothHost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), BluetoothHostActivity.class);
                startActivity(i);
            }
        });

        mainMenuBluetoothJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), BluetoothJoinActivity.class);
                startActivity(i);
            }
        });

        /*mainMenuRules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),RulesActivity.class );
                startActivity(i);
            }
        });

        mainMenuSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),SettingsActivity.class );
                startActivity(i);
            }
        });*/


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(getApplicationContext(),SettingsActivity.class );
            startActivity(i);
        } else if (id == R.id.action_rules){
            Intent i = new Intent(getApplicationContext(),RulesActivity.class );
            startActivity(i);
        } else if (id == R.id.action_exit){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
