package hr.tvz.gmg.unlino.entities;

import java.util.UUID;

/**
 * Created by Robin on 07-Jun-16.
 */
public interface Constants {

    // Host/Join constants
    int REQUEST_ENABLE_BT = 1;
    int REQUEST_DISCOVERABLE_BT = 2;

    // Socket info
    String NAME = "SERVERAPP";
    UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    // Message types sent from the BluetoothChatService Handler
    int MESSAGE_STATE_CHANGE = 1;
    int MESSAGE_READ = 2;
    int MESSAGE_WRITE = 3;
    int MESSAGE_DEVICE_NAME = 4;
    int MESSAGE_TOAST = 5;

    // Settings
    String SHARED_PREFERENCES_KEY = "hr.tvz.gmg.unlino.SPYFALL";
    long COUNT_DOWN_INTERVAL = 1000; // timer

    String spKeyUsername = "spUsername";
    String defaultUsername = "Guest";

    String spKeyDuration = "spDuration";
    int defaultDuration = 5;

    String spKeyLanguage = "spLanguage";
    String defaultLanguage = "English";

}