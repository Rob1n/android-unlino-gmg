package hr.tvz.gmg.unlino.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hr.tvz.gmg.unlino.R;
import hr.tvz.gmg.unlino.entities.Constants;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SettingsActivity extends AppCompatActivity {

    Context mContext;

    private EditText etUsername;
    private EditText etDuration;
    private Button btnLanguage;
    private Button btnSaveChanges;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // custom font
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/PTSansNarrow.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarSettings);
        toolbar.setTitleTextAppearance(this, R.style.titleText);
        setSupportActionBar(toolbar);

        mContext = this;

        etUsername = (EditText) findViewById(R.id.editTextSettingsUsername);
        etDuration = (EditText) findViewById(R.id.editTextSettingsDuration);
        btnLanguage = (Button) findViewById(R.id.buttonSettingsLanguage);
        btnSaveChanges = (Button) findViewById(R.id.buttonSettingsSaveChanges);

        sharedPreferences = this.getSharedPreferences(Constants.SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE);

        // read values
        String username = sharedPreferences.getString(Constants.spKeyUsername, Constants.defaultUsername);
        int duration = sharedPreferences.getInt(Constants.spKeyDuration, Constants.defaultDuration);
        final String language = sharedPreferences.getString(Constants.spKeyLanguage, Constants.defaultLanguage);

        // set default values
        etUsername.setText(username);
        etDuration.setText(Integer.toString(duration));
        btnLanguage.setText(language);

        btnLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] tmp1 = mContext.getResources().getStringArray(R.array.deck_locations);
                List<String> tmp2  = Arrays.asList(tmp1);
                final ArrayList<String> languages = new ArrayList<>(tmp2);

                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);
                builder.setTitle("Choose your preferred language")
                        .setItems(R.array.languageOptions, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // The 'which' argument contains the index position
                                // of the selected item
                                Toast.makeText(SettingsActivity.this, "Selected: "+languages.get(which), Toast.LENGTH_SHORT).show();
                                //TODO change language locale, reload app
                                btnLanguage.setText(languages.get(which));
                            }
                        }).create().show();

            }
        });

        btnSaveChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPreferences.edit();

                editor.putString(Constants.spKeyUsername, etUsername.getText().toString());

                int dur = Integer.parseInt(etDuration.getText().toString());
                editor.putInt(Constants.spKeyDuration, dur);

                editor.putString(Constants.spKeyLanguage, btnLanguage.getText().toString());

                editor.commit();
                finish();
            }
        });

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
