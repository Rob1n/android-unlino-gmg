package hr.tvz.gmg.unlino.entities;

/**
 * Created by Robin on 08-Jun-16.
 */
public class TestDevice {
    String name;
    String address;

    public TestDevice(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
