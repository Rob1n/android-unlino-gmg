package hr.tvz.gmg.unlino.entities;

/**
 * Created by Robin on 12-Apr-16.
 */
public class Card {
    public enum Color {
        BLUE, GREEN, RED, YELLOW, BLACK
    }

    public enum Type {
        NUMBER, SKIP, DRAW_TWO, REVERSE, WILD, WILD_DRAW_FOUR
    }

    private Color color;
    private Type type;
    private int value;

    public Card(Color color, Type type, int value) {
        this.color = color;
        this.type = type;
        this.value = value;
    }

    public Color getColor() {
        return color;
    }

    public Type getType() {
        return type;
    }

    public int getValue() {
        return value;
    }
}
