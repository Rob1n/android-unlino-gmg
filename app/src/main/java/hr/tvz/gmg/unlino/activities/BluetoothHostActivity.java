package hr.tvz.gmg.unlino.activities;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import hr.tvz.gmg.unlino.R;
import hr.tvz.gmg.unlino.adapters.PairedDevicesAdapter;
import hr.tvz.gmg.unlino.entities.Constants;
import hr.tvz.gmg.unlino.networking.ParcelableBluetoothDevices;
import hr.tvz.gmg.unlino.networking.ParcelableBluetoothSockets;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BluetoothHostActivity extends AppCompatActivity {
    //@BindView(R.id.btnStartListening) Button btnStartListening;
    //@BindView(R.id.listViewHostPairedDevices) ListView listViewPairedDevices;

    ListView listViewPairedDevices;
    Button btnStartListening;
    Button btnStartGame;

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothServerSocket mmServerSocket;

    ArrayList<BluetoothDevice> connectedDevices;
    ArrayList<BluetoothSocket> connectedSockets;
    private PairedDevicesAdapter connectedDevicesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // custom font
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/PTSansNarrow.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_bluetooth_host);
        //ButterKnife.bind(this);

        listViewPairedDevices = (ListView) findViewById(R.id.listViewHostPairedDevices);
        btnStartListening = (Button) findViewById(R.id.btnStartListening);
        btnStartGame = (Button) findViewById(R.id.btnHostStartGame);

        // initialize bluetooth
        this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null){
            // bluetooth not supported
            Toast.makeText(getApplicationContext(), "Bluetooth is not supported on this device", Toast.LENGTH_SHORT).show();
            finish();
        }

        // initializing listview of paired devices
        connectedSockets = new ArrayList<>();
        connectedDevices = new ArrayList<>();
        connectedDevicesAdapter = new PairedDevicesAdapter(getApplicationContext(), connectedDevices);
        listViewPairedDevices.setAdapter(connectedDevicesAdapter);

        // enable bluetooth if disabled
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, Constants.REQUEST_ENABLE_BT);
        }

        // make device discoverable
        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
        startActivityForResult(discoverableIntent, Constants.REQUEST_DISCOVERABLE_BT);

        btnStartListening.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AcceptThread acceptThread = new AcceptThread();
                acceptThread.start();
            }
        });

        btnStartGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO return value when finished testing
                if (connectedDevices.size() < 0){
                    Toast.makeText(BluetoothHostActivity.this, "Nema dovoljno igrača!", Toast.LENGTH_SHORT).show();
                } else {
                    Intent i = new Intent(getApplicationContext(), GameHostActivity.class);
                    i.putExtra("hostConnectedDevices", new ParcelableBluetoothDevices(connectedDevices));
                    i.putExtra("hostConnectedSockets", new ParcelableBluetoothSockets(connectedSockets));
                    startActivity(i);
                }
            }
        });

        //bluetoothServerSocket.accept();

    }

    private void manageConnectedSocket(BluetoothSocket socket){
        this.connectedDevices.add(socket.getRemoteDevice());
        this.connectedSockets.add(socket);
        connectedDevicesAdapter.notifyDataSetChanged();
        Toast.makeText(getApplicationContext(), "device connected!", Toast.LENGTH_SHORT).show();
    }

    public synchronized ArrayList<BluetoothDevice> getBtDevices(){
        return connectedDevices;
    }

    public synchronized ArrayList<BluetoothSocket> getBtSockets(){
        return connectedSockets;
    }

    /*private void updateClientListWithPairedDevices() {
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                // Add the name and address to an array adapter to show in a ListView
                ParcelableBluetoothDevice parcelableBluetoothDevice = new ParcelableBluetoothDevice(device);
                this.arrayList.add(parcelableBluetoothDevice);
            }
        }
    }*/

    private class AcceptThread extends Thread {
        //private final BluetoothServerSocket mmServerSocket;

        public AcceptThread() {
            mmServerSocket = null;
            // Use a temporary object that is later assigned to mmServerSocket,
            // because mmServerSocket is final

            BluetoothServerSocket tmp = null;
            try {
                // MY_UUID is the app's UUID string, also used by the client code
                tmp = mBluetoothAdapter.listenUsingRfcommWithServiceRecord(Constants.NAME, Constants.MY_UUID);
            } catch (IOException e) { }
            mmServerSocket = tmp;
        }

        public void run() {
            BluetoothSocket socket = null;
            // Keep listening until exception occurs or a socket is returned
            while (true) {
                try {
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    break;
                }
                // If a connection was accepted
                if (socket != null) {
                    // Do work to manage the connection (in a separate thread)
                    manageConnectedSocket(socket);
                    try {
                        mmServerSocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
        }

        // Will cancel the listening socket, and cause the thread to finish
        public void cancel() {
            try {
                mmServerSocket.close();
            } catch (IOException e) { }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case (Constants.REQUEST_ENABLE_BT):
                if (resultCode == RESULT_OK){
                    // bluetooth successfully enabled
                }
                break;
            case (Constants.REQUEST_DISCOVERABLE_BT):
                if (resultCode == RESULT_CANCELED){
                    // user refused to make device discoverable
                }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}