package hr.tvz.gmg.unlino.networking;

import android.bluetooth.BluetoothSocket;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Robin on 06-Jun-16.
 */
public class ParcelableBluetoothSockets implements Parcelable {

    ArrayList<BluetoothSocket> sockets;

    public ParcelableBluetoothSockets(ArrayList<BluetoothSocket> sockets) {
        this.sockets = sockets;
    }

    public ArrayList<BluetoothSocket> getSockets() {
        return sockets;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.sockets);
    }

    protected ParcelableBluetoothSockets(Parcel in) {
        this.sockets = new ArrayList<BluetoothSocket>();
        in.readList(this.sockets, BluetoothSocket.class.getClassLoader());
    }

    public static final Parcelable.Creator<ParcelableBluetoothSockets> CREATOR = new Parcelable.Creator<ParcelableBluetoothSockets>() {
        @Override
        public ParcelableBluetoothSockets createFromParcel(Parcel source) {
            return new ParcelableBluetoothSockets(source);
        }

        @Override
        public ParcelableBluetoothSockets[] newArray(int size) {
            return new ParcelableBluetoothSockets[size];
        }
    };
}
