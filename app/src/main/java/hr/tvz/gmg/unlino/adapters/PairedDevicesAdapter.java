package hr.tvz.gmg.unlino.adapters;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import hr.tvz.gmg.unlino.R;
import hr.tvz.gmg.unlino.entities.TestDevice;

/**
 * Created by Robin on 06-Jun-16.
 */
public class PairedDevicesAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<BluetoothDevice> bluetoothDevices;

    public PairedDevicesAdapter(Context context, ArrayList<BluetoothDevice> bluetoothDevices){
        this.mContext = context;
        this.bluetoothDevices = bluetoothDevices;
    }

    @Override
    public int getCount() {
        return bluetoothDevices.size();
        //return 4;
    }

    @Override
    public BluetoothDevice getItem(int position) {
        return bluetoothDevices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null){
            LayoutInflater vi;
            vi = LayoutInflater.from(mContext);
            v = vi.inflate(R.layout.list_item_paired_device, null);
        }

        // testing listview
        /*ArrayList<TestDevice> test = new ArrayList<>();
        test.add(new TestDevice("Robifon", "0003C92DB48F"));
        test.add(new TestDevice("PhoneA", "004AA51241F1"));
        test.add(new TestDevice("Nexus9", "000014211FCC"));
        test.add(new TestDevice("HuaweiUređaj", "F94BD29C3000"));
        TestDevice bD = test.get(position);*/

        BluetoothDevice bD = getItem(position);
        if (bD != null){
            TextView textViewName = (TextView) v.findViewById(R.id.itemListPairedDeviceName);
            TextView textViewAddress = (TextView) v.findViewById(R.id.itemListPairedDeviceAddress);

            if (textViewName != null){
                textViewName.setText(bD.getName());
                textViewName.setTextColor(Color.BLACK);
            }

            if (textViewAddress != null){
                textViewAddress.setText(bD.getAddress());
                textViewAddress.setTextColor(Color.BLUE);
            }

        }

        return v;
    }



}
